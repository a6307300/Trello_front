/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getComments = createAsyncThunk(
    'comments/getcomments',
    async (task:string, { rejectWithValue, dispatch}) => {
        try {
            const response = await axios.get(`http://localhost:3003/task/comment/${task}`, {
                headers: {
                    authorization: `${localStorage.getItem('token')}`,
                },
            });
            const arrayComments=Array.from(response.data);
            dispatch(getCommentState(arrayComments));
            return arrayComments;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface ICommentData {
    key?: number,
    comment?: string,
    authorID?:number,
    authorName?:string,
    authorAva?: string,
    id?: number,
    task?:number,
  }

export const addComment = createAsyncThunk(
    'comments/addcomment',
    async (commentData:ICommentData, { rejectWithValue, dispatch }) => {
        try {
            const dataComment = JSON.stringify(commentData);
            const response = await axios.post(
                `http://localhost:3003/task/comment/${commentData.task}`,
                dataComment,
                {
                    headers: {
                        authorization: `${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            response.data.author.avatar=response.data.author.avatar? 
                'http://localhost:3003/'+response.data.author.avatar.slice(41): 'images/placeholder.png';
            dispatch(addCommentState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const deleteComment = createAsyncThunk(
    'comments/deletecomment',
    async (id:number, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.delete(
                `http://localhost:3003/task/comments/${id}`,
                {
                    headers: {
                        'Content-Type': 'mulpipart/form-data',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(deleteCommentState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const changeComment = createAsyncThunk(
    'comments/changecomment',
    async (dataComment: ICommentData, { rejectWithValue, dispatch }) => {
        try {
            const commentData = JSON.stringify(dataComment);
            const response = await axios.patch(
                `http://localhost:3003/task/comments/edit/${dataComment.id}`,
                commentData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(changeCommentState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

type authorData = {fullName: string, avatar: string}
type SliceStateComment = {id:number, comment:string, order:number, taskID: number, userID: string, author:authorData}[];

const commentSlice = createSlice({
    name: 'comments',
    initialState: {
        comments:[] as SliceStateComment,
    },
    reducers: {
        getCommentState(state, action) {
            state.comments = action.payload;
            state.comments.map(comment => comment.author.avatar=comment.author.avatar? 
                'http://localhost:3003/'+comment.author.avatar.slice(41): 'images/placeholder.png');

        },
        addCommentState(state, action) {
            state.comments.unshift(action.payload);
            
        },
        deleteCommentState(state, action) {
            state.comments = state.comments.filter(comment => comment.id != action.payload);
        },
        changeCommentState(state, action) {
            // @ts-ignore
            state.comments.find(comment => comment.id === action.payload.id).comment = action.payload.comment;
        },
    },
});

export const { getCommentState, addCommentState, deleteCommentState, changeCommentState} =   commentSlice.actions;

export default commentSlice.reducer;
