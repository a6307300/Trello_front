/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getBoards = createAsyncThunk(
    'boards/getboards',
    async (_, { rejectWithValue, dispatch}) => {
        try {
            const response = await axios.get('http://localhost:3003/boards', {
                headers: {
                    authorization: `${localStorage.getItem('token')}`,
                },
            });
            const arrayBoards=Array.from(response.data);
            dispatch(getBoardsState(arrayBoards));
            return arrayBoards;
        } catch (error: any) {
            if (error.response.status == '401') {
                localStorage.setItem('isAuth', 'false');
            }
            return rejectWithValue(error.message);
        }
    }
);

interface IBoardData {
    boardName?: string,
    id?: string,
    contributor?:string,
    contributorId?:number,
  }

export const addBoard = createAsyncThunk(
    'boards/addboard',
    async (boardData:IBoardData, { rejectWithValue, dispatch }) => {
        try {
            const dataBoard = JSON.stringify(boardData);
            const response = await axios.post(
                'http://localhost:3003/boards',
                dataBoard,
                {
                    headers: {
                        authorization: `${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(addBoardState(response.data));
            return response.data;
        } catch (error: any) {
            if (error.response.status == '401') {
                localStorage.setItem('isAuth', 'false');
            }
            if (error.response.status == '400') {
                alert(
                    'Имя новой доски не должно повторять уже имеющиеся'
                );
            }
            return rejectWithValue(error.message);
        }
    }
);                localStorage.setItem('isAuth', 'false');

export const deleteBoard = createAsyncThunk(
    'boards/deleteboard',
    async (_, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.delete(
                `http://localhost:3003/boards/board/id/${localStorage.getItem('boardId')}`,
                {
                    headers: {
                        'Content-Type': 'mulpipart/form-data',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(deleteBoardState(response.data));
            return response.data;
        } catch (error: any) {
            if (error.response.status == '401') {
                localStorage.setItem('isAuth', 'false');
            }
            return rejectWithValue(error.message);
        }
    }
);

export const renameBoard = createAsyncThunk(
    'board/renameboard',
    async (dataBoard: IBoardData, { rejectWithValue, dispatch }) => {
        try {
            const boardData = JSON.stringify(dataBoard);
            const response = await axios.patch(
                `http://localhost:3003/boards/board/id/${dataBoard.id}`,
                boardData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(renameBoardState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const shareBoard = createAsyncThunk(
    'board/shareboard',
    async (dataBoard: IBoardData, { rejectWithValue, dispatch }) => {
        try {
            const boardData = JSON.stringify(dataBoard);
            const response = await axios.patch(
                `http://localhost:3003/boards/contribute/${dataBoard.id}`,
                boardData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            alert(`Вы дали доступ к доске ${localStorage.getItem('boardName')} пользователю с электронной почтой ${dataBoard.contributor}`);
            dispatch(shareBoardState(response.data));
            return response.data;
        } catch (error: any) {
            if (error.response.status=='400') {
                alert('Этот контрибьютор уже заявлен в этой доске');

            }
            return rejectWithValue(error.message);
        }
    }
);

type authorData = {fullName: string, avatar: string}
type SliceStateBoard = {id:number, boardName:string, owner:number, contributors:Array<string>, user:authorData}[];

const boardSlice = createSlice({
    name: 'boards',
    initialState: {
        boards:[] as SliceStateBoard,
        
    },
    reducers: {
        getBoardsState(state, action) {
            state.boards = action.payload;
        },
        addBoardState(state, action) {
            state.boards.push(action.payload);
        },
        deleteBoardState(state, action) {
            state.boards = state.boards.filter(board => board.id != action.payload);
        },
        renameBoardState(state, action) {
            // @ts-ignore
            state.boards.find(board => board.id === action.payload.id).boardName = action.payload.boardName;
        },
        shareBoardState(state, action) {
            // @ts-ignore
            state.boards.find(board => board.id === action.payload.id).contributors = action.payload.contributors;
        },
    }
});
export const { getBoardsState, addBoardState, deleteBoardState, renameBoardState, shareBoardState } =   boardSlice.actions;

export default boardSlice.reducer;
