/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getusersList = createAsyncThunk(
    'usersList/getusersList',
    async (_, { rejectWithValue, dispatch}) => {
        try {
            const response = await axios.get('http://localhost:3003/user/all', {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Access-Control-Allow-Origin': '*',
                },
            });
            type StateUsers = {id:number, display:string, fullName:string};
            const ttt = response.data.map((user:StateUsers) => user.display=user.fullName); 
            const arrayUsers=Array.from(response.data);
            dispatch(getUsersListState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

type SliceStateUsers = {id:number, display:string}[];

const usersListSlice = createSlice({
    name: 'usersList',
    initialState: {
        usersList:[] as SliceStateUsers,
        
    },
    reducers: {
        getUsersListState(state, action) {
            state.usersList = action.payload;
        },
    }
});
export const { getUsersListState } =   usersListSlice.actions;

export default usersListSlice.reducer;
