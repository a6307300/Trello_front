/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice} from '@reduxjs/toolkit';

const taskModalSlice = createSlice({
    name: 'taskModal',
    initialState: {
        taskModal:{taskId:100, taskName:' ', description:' ', range:5, column: ' ', act:false}
    },
    reducers: {
        setActState(state, action) {
            state.taskModal.act = action.payload;
        },
        setTaskId(state, action) {
            state.taskModal.taskId=action.payload;
        },
        setTaskName(state, action) {
            state.taskModal.taskName=action.payload;
        },
        setDescript(state, action) {
            state.taskModal.description=action.payload;
        },
        setRange(state, action) {
            state.taskModal.range=action.payload;
        },
        setColumn(state, action) {
            state.taskModal.column=action.payload;
        },
    },
});

export const { setActState, setTaskId, setTaskName, setDescript,setRange, setColumn} =   taskModalSlice.actions;

export default taskModalSlice.reducer;
