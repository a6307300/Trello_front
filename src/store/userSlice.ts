/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
interface IDataUser {
  fullName: string;
  email: string;
  password: string;
}

interface IDataUserShort {
  email: string;
  password: string;
}

export const postUser = createAsyncThunk(
    'users/postuser',
    async (dataUser: IDataUser, { rejectWithValue }) => {
        try {
            const userData = JSON.stringify(dataUser);
            const response = await axios.post(
                'http://localhost:3003/user',
                userData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            localStorage.setItem('name', response.data.userTarget.fullName);
            localStorage.setItem('email', response.data.userTarget.email);
            localStorage.setItem('id', response.data.userTarget.id);
            localStorage.setItem('ava', 'images/placeholder.png');
            localStorage.setItem('token', response.data.token);
            return response.data;
        } catch (error: any) {
            const currentError = rejectWithValue(error.message).payload;
            if (currentError === 'Request failed with status code 400') {
                alert(
                    'Ошибка регистрации! Введите корректный e-mail и пароль от 3 до 9 символов'
                );
            }
            return rejectWithValue(error.message);
        }
    }
);

export const loginUser = createAsyncThunk(
    'users/loginuser',
    async (dataUser: IDataUserShort, { rejectWithValue }) => {
        try {
            localStorage.clear();
            const userData = JSON.stringify(dataUser);
            const response = await axios.post(
                'http://localhost:3003/user/auth',
                userData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('sharedBoards', '');
            localStorage.setItem('name', response.data.candidate.fullName);
            localStorage.setItem('email', response.data.candidate.email);
            localStorage.setItem('id', response.data.candidate.id);
            localStorage.setItem('ava', !response.data.candidate.avatar?'images/placeholder.png':
                'http://localhost:3003/'+response.data.candidate.avatar.slice(41));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const checkLogin = createAsyncThunk(
    'users/checklogin',
    async (_, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.get('http://localhost:3003/user', {
                headers: {
                    authorization: `${localStorage.getItem('token')}`,
                },
            });
            return response;
        } catch (error: any) {
            if (error.response.status=='401') {
                dispatch(changeIsAuth(false));
                localStorage.clear();
            }
            localStorage.clear();
            return rejectWithValue(error.message);
        }
    }
);

export const modifyUser = createAsyncThunk(
    'users/modifyuser',
    async (dataUser: IDataUserShort, { rejectWithValue, dispatch }) => {
        try {
            const userData = JSON.stringify(dataUser);
            const response = await axios.patch(
                `http://localhost:3003/user/${localStorage.getItem('id')}`,
                userData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            if (response.data.avatar) {
                const adress = response.data.avatar.slice(41);
                const adressProxy = 'http://localhost:3003/' + adress;
                localStorage.setItem('ava', adressProxy);
                dispatch(changeUserAva(response.data.avatar));     
            }
            localStorage.setItem('name', response.data.fullName);
            localStorage.setItem('email', response.data.email);
            localStorage.setItem('id', response.data.id);
            dispatch(changeUser(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const uploadAvatar = createAsyncThunk(
    'users/uploadavatar',
    async (data: any, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.post(
                `http://localhost:3003/user/${localStorage.getItem('id')}/avatar`,
                data,
                {
                    headers: {
                        'Content-Type': 'mulpipart/form-data',
                        authorization: `${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(changeUserAva(response.data));
            const adress = response.data.slice(41);
            const adressProxy = 'http://localhost:3003/' + adress;
            localStorage.setItem('ava', adressProxy);
            return response;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

const userSlice = createSlice({
    name: 'user',
    initialState: {
        status: null,
        error: null,
        isAuth: false,
        token:'',
        user: {
            name: '',
            email: '',
            id: '',
            ava: '',
        },
    },
    reducers: {
        changeToken(state, action) {
            state.token = action.payload;
            localStorage.setItem('token', state.token);
        },
        changeIsAuth(state, action) {
            state.isAuth = action.payload;
        },
        changeUser(state, action) {
            state.user.name = action.payload.fullName;
            state.user.email = action.payload.email;
            state.user.id = action.payload.id;
            state.user.ava = 'images/placeholder.png';
        },
        changeUserAva(state, action) {
            const adress = action.payload.slice(41);
            const adressProxy = 'http://localhost:3003/' + adress;
            state.user.ava = adressProxy;
        },

        logoutUser(state) {
            state.user.name = '';
            state.user.email = '';
            state.user.id = '';
            state.user.ava = '';
            localStorage.clear();
        },
    },
});
export const { changeIsAuth, changeUser, changeUserAva, logoutUser, changeToken } =
  userSlice.actions;

export default userSlice.reducer;
