import {StateType} from 'typesafe-actions';

declare module 'typesafe-actions' {
  export type Store = StateType<typeof import('./index').default>;

  export type MyRootState = StateType<typeof import('./userSlice').default>;
  
  export type BoardRootState = StateType<typeof import('./boardSlice').default>;

  export type ColumnRootState = StateType<typeof import('./columnSlice').default>;

  export type TaskRootState = StateType<typeof import('./taskSlice').default>;

  export type CommentRootState = StateType<typeof import('./commentSlice').default>;

  export type SharedRootState = StateType<typeof import('./sharedBoardSlice').default>;

  export type ModalRootState = StateType<typeof import('./taskModalSlice').default>;

  export type UsersRootState = StateType<typeof import('./usersListSlice').default>;

  interface Types {
    RootAction: RootAction;
  }
}