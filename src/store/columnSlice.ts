/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getColumns = createAsyncThunk(
    'columns/getcolumns',
    async (_, { rejectWithValue, dispatch}) => {
        try {
            const response = await axios.get(`http://localhost:3003/boards/board/${localStorage.getItem('board')}`, {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            });
            const arrayColumns=Array.from(response.data);
            dispatch(getColumnsState(arrayColumns));
            return arrayColumns;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface IColumnData {
    columnName: string,
    board?:string,
    id?:number,
  }

export const addColumn = createAsyncThunk(
    'columns/addcolumn',
    async (columnData:IColumnData, { rejectWithValue, dispatch }) => {
        try {
            const dataColumn = JSON.stringify(columnData);
            const response = await axios.post(
                `http://localhost:3003/boards/board/${columnData.board}`,
                dataColumn,
                {
                    headers: {
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(addColumnState(response.data));
            return response.data;
        } catch (error: any) {
            if (error.response.status=='400') {
                alert('У Вас уже есть колонка с таким именем');
            }
            return rejectWithValue(error.message);
        }
    }
);

export const renameColumn = createAsyncThunk(
    'column/renamecolumn',
    async (dataColumn: IColumnData, { rejectWithValue, dispatch }) => {
        try {
            const columnData = JSON.stringify(dataColumn);
            const response = await axios.patch(
                `http://localhost:3003/boards/board/column/id/${dataColumn.id}`,
                columnData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(renameColumnState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const deleteColumn = createAsyncThunk(
    'columns/deletecolumn',
    async (columnId:number, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.delete(
                `http://localhost:3003/boards/board/column/id/${columnId}`,
                {
                    headers: {
                        'Content-Type': 'mulpipart/form-data',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(deleteColumnState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface IReplaceData {
    current: number,
    replaced:number,
  }

export const replaceColumn = createAsyncThunk(
    'columns/replacecolumn',
    async (columnData:IReplaceData, { rejectWithValue, dispatch }) => {
        try {
            const dataColumn = JSON.stringify(columnData);
            const response = await axios.post(
                'http://localhost:3003/boards/board/column/replace',
                dataColumn,
                {
                    headers: {
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            const arrayColumns=Array.from(response.data);
            dispatch(getColumnsState(arrayColumns));
            return arrayColumns;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

type SliceStateColumns = {id:number, columnName:string, boardID:number}[];

const columnSlice = createSlice({
    name: 'columns',
    initialState: {
        columns:[] as SliceStateColumns,
    },
    reducers: {
        getColumnsState(state, action) {
            state.columns = action.payload;
        },
        addColumnState(state, action) {
            state.columns.push(action.payload);
        },
        deleteColumnState(state, action) {
            state.columns = state.columns.filter(column => column.id != action.payload);
        },
        renameColumnState(state, action) {
            // @ts-ignore
            state.columns.find(column => column.id === action.payload.id).columnName = action.payload.columnName;
        },

    },
});
export const { getColumnsState, addColumnState, deleteColumnState, renameColumnState} =   columnSlice.actions;

export default columnSlice.reducer;
