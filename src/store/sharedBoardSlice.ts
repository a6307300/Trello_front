/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';


export const getSharedBoards = createAsyncThunk(
    'boards/getsharedboards',
    async (_, { rejectWithValue, dispatch}) => {
        try {
            const dataBoard = {
                contributor: localStorage.getItem('email'),
            };
            const boardData = JSON.stringify(dataBoard);
            const response = await axios.post(
                'http://localhost:3003/boards/contributor', 
                boardData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                });
            const arrayBoards=Array.from(response.data);
            dispatch(getSharedBoardsState(arrayBoards));
            return arrayBoards;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);
type authorData = {fullName: string, avatar: string}
type SliceStateBoard = {id:number, boardName:string, owner:number, contributors:Array<string>, user:authorData}[];

const sharedSlice = createSlice({
    name: 'shared',
    initialState: {
        shared:[] as SliceStateBoard,
        
    },
    reducers: {
        getSharedBoardsState(state, action) {
            state.shared = action.payload;
            state.shared.map(share => share.user.avatar=share.user.avatar? 
                'http://localhost:3003/'+share.user.avatar.slice(41): 'images/placeholder.png');
        },
        


    },
});
export const { getSharedBoardsState } =   sharedSlice.actions;

export default sharedSlice.reducer;
