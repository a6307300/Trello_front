import { configureStore } from '@reduxjs/toolkit';
import userReducer from  './userSlice';
import boardReducer from './boardSlice';
import columnReducer from './columnSlice';
import taskReducer from './taskSlice';
import commentReducer from './commentSlice';
import sharedReducer from './sharedBoardSlice';
import taskModalReducer from './taskModalSlice';
import usersListReducer from './usersListSlice';


export default configureStore ({
    reducer:{
        user: userReducer,
        boards: boardReducer,
        columns: columnReducer,
        tasks: taskReducer,
        comments: commentReducer,
        shared: sharedReducer,
        taskModal: taskModalReducer,
        usersList: usersListReducer,
    }
    
});