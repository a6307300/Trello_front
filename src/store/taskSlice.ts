/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getTasks = createAsyncThunk(
    'taks/gettasks',
    async (boardId:string, { rejectWithValue, dispatch}) => {
        try {
            const response = await axios.get(`http://localhost:3003/boards/board/column/${boardId}`, {
                headers: {
                    authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            });
            const arrayTasks=Array.from(response.data);
            dispatch(getTasksState(arrayTasks));
            return arrayTasks;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface ITaskData {
    taskName: string,
    column:number,
  }

export const addTask = createAsyncThunk(
    'tasks/addtask',
    async (taskData:ITaskData, { rejectWithValue, dispatch }) => {
        try {
            const dataTask = JSON.stringify(taskData);
            const response = await axios.post(
                `http://localhost:3003/boards/board/column/newcolumn/${taskData.column}`,
                dataTask,
                {
                    headers: {
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(addTaskState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const deleteTask = createAsyncThunk(
    'tasks/deletetask',
    async (id:number, { rejectWithValue, dispatch }) => {
        try {
            const response = await axios.delete(
                `http://localhost:3003/boards/board/column/task/${id}`,
                {
                    headers: {
                        'Content-Type': 'mulpipart/form-data',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(deleteTaskState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface IDataTask {
    taskName?: string,
    description?: string,
    id: number,
    range?: string,
}

export const renameTask = createAsyncThunk(
    'tasks/renametask',
    async (dataTask: IDataTask, { rejectWithValue, dispatch }) => {
        try {
            const taskData = JSON.stringify(dataTask);
            const response = await axios.patch(
                `http://localhost:3003/boards/board/column/task/${dataTask.id}`,
                taskData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(renameTaskState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const describeTask = createAsyncThunk(
    'tasks/renametask',
    async (dataTask: IDataTask, { rejectWithValue, dispatch }) => {
        try {
            const taskData = JSON.stringify(dataTask);
            const response = await axios.patch(
                `http://localhost:3003/boards/board/column/task/${dataTask.id}`,
                taskData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(describeTaskState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

export const rangeTask = createAsyncThunk(
    'tasks/ratetask',
    async (dataTask: IDataTask, { rejectWithValue, dispatch }) => {
        try {
            const taskData = JSON.stringify(dataTask);
            const response = await axios.patch(
                `http://localhost:3003/boards/board/column/task/${dataTask.id}`,
                taskData,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        authorization: `Bearer ${localStorage.getItem('token')}`,
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            dispatch(rangeTaskState(response.data));
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);

interface IReplaceData {
    current: number,
    replaced:number,
    column?: number,
    board?: string,
  }

export const replaceTask = createAsyncThunk(
    'tasks/replacetask',
    async (taskData:IReplaceData, { rejectWithValue, dispatch }) => {
        try {
            const dataTask = JSON.stringify(taskData);
            const response = await axios.patch(
                'http://localhost:3003/boards/board/column/replaced',
                dataTask,
                {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                    },
                }
            );
            const arrayTasks=Array.from(response.data);
            dispatch(getTasksState(arrayTasks));
            return arrayTasks;
        } catch (error: any) {
            return rejectWithValue(error.message);
        }
    }
);
type SliceStateTask = {id:number, taskName:string, description:string, range:number, columnID: number}[];

const taskSlice = createSlice({
    name: 'tasks',
    initialState: {
        tasks:[] as SliceStateTask,
    },
    reducers: {
        getTasksState(state, action) {
            state.tasks = action.payload;
        },
        addTaskState(state, action) {
            state.tasks.push(action.payload);
            state.tasks=state.tasks.sort((a,b) => a.range>b.range?-1:1);
        },
        deleteTaskState(state, action) {
            state.tasks = state.tasks.filter(task => task.id != action.payload);
        },
        renameTaskState(state, action) {
            // @ts-ignore
            state.tasks.find(task => task.id === action.payload.id).taskName = action.payload.taskName;
        },
        describeTaskState(state, action) {
            // @ts-ignore
            state.tasks.find(task => task.id === action.payload.id).description = action.payload.description;
        },
        rangeTaskState(state, action) {
            // @ts-ignore
            state.tasks.find(task => task.id === action.payload.id).range = action.payload.range;
            state.tasks=state.tasks.sort((a,b) => a.range>b.range?-1:1);
        },
    },
});

export const { getTasksState, addTaskState, deleteTaskState, renameTaskState, describeTaskState, rangeTaskState} =   taskSlice.actions;

export default taskSlice.reducer;
