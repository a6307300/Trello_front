/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, FC } from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { ColumnRootState } from 'typesafe-actions';
import PrivateHeader from './formComponents/PrivateHeader';
import Header from './Header';
import ColumnItem from './ColumnItem';
import ModalTask from './ModalTask';
import {getColumns, addColumn, replaceColumn} from '../store/columnSlice';
import { shareBoard } from '../store/boardSlice';
import { replaceTask } from '../store/taskSlice';
import { getusersList } from '../store/usersListSlice';

const Board: FC = () => {

    const dispatch = useDispatch();

    const columns = useSelector((state:ColumnRootState) => state.columns);

    useEffect(() => {
        dispatch(getColumns());
        dispatch(getusersList());
    }, []);

    const hidden = Boolean(localStorage.getItem('hideShared'));

    const [inputColumn, setInputColumn] = useState<boolean>(false);
    const [inputColumnText, setInputColumnText] = useState<string>(' ');
   
    const [currentColumn, setCurrentColumn] = useState<number>(0);

    const [currentTaskColumn, setCurrentTaskColumn] = useState<number>(0);
    const [currentTask, setCurrentTask] = useState<number>(0);

    const dragTaskOverHandler = (e:React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
    };
    const dragTaskStartHandler = (e:React.DragEvent<HTMLDivElement>, task:number, column: number) => {
        setCurrentColumn(column);
        setCurrentTask(task);
    };
    const dragTaskEndHandler = (e:DragEvent) => {
        e.preventDefault();
    };
    const dropTaskHandler = (e:React.DragEvent<HTMLDivElement>, task: number, column:number) => {
        e.preventDefault();
        const tasksDD ={
            current: currentTask,
            replaced:task,
            column: column,
            board: localStorage.getItem('board') as string,
        };
        dispatch(replaceTask(tasksDD));
    };
    const dragOverHandler = (e:React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
    };
    const dragStartHandler = (e:React.DragEvent<HTMLDivElement>, column: number) => {
        setCurrentColumn(column);
    };
    const dragEndHandler = (e:DragEvent) => {
        e.preventDefault();
    };
    const dropHandler = (e:React.DragEvent<HTMLDivElement>, column:number) => {
        e.preventDefault();
        const columnsDD ={
            current: currentColumn,
            replaced:column,
        };
        dispatch(replaceColumn(columnsDD));
    };
    const handleNewColumn = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputColumnText(target.value);
    };  
    const showNewColumn = () => {
        setInputColumn(true);
    };
    const EnterClick = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            createColumn(e);
        }
        if (e.key === 'Escape') {
            setInputColumnText('');
            setInputColumn(false);
        }
    };
    interface IColumnData {
        columnName: string,
      board:string,
    }
    const createColumn = (e:React.SyntheticEvent) => {
        e.preventDefault();
        if (inputColumnText!==''){
            const idBoard = localStorage.getItem('board') as string;
            const columnData: IColumnData = {
                columnName: inputColumnText,
                board: idBoard,
            };
            dispatch(addColumn(columnData));
            setInputColumnText('');
            setInputColumn(false);
        } else {
            alert('Имя новой колонки не может быть пустым');
            setInputColumn(false);
        }
    };
    const sharedBoard = (e:React.SyntheticEvent) => {
        e.preventDefault();
        const mail = prompt('Введите электронную почту Вашего контрибьютера','');
        if (mail) {
            const boardData = {
                contributor: mail,
                id: localStorage.getItem('board') as string,
            };
            dispatch(shareBoard(boardData));
        } else {
            alert('Электронная почта указана неверно');
        }
    };
    return(
        <BoardSpace>
            <Header/>
            <PrivateHeader 
                inputTextName={localStorage.getItem('name') as string} 
                sharedBoard={sharedBoard}
            />
            <ColumnsSpace>
                {Object.values(columns).flat().map((column) => {
                    return (
                        <ColumnItem 
                            key={column.id}
                            columnId={column.id}
                            columnName={column.columnName}
                            currentColumn={currentColumn}
                            currentTask={currentTask}
                            setCurrentColumn={setCurrentColumn}
                            setCurrentTask={setCurrentTask}
                            setCurrentTaskColumn={setCurrentTaskColumn}
                            dragStartHandler={dragStartHandler}
                            dragEndHandler={dragEndHandler}
                            dragOverHandler={dragOverHandler}
                            dropHandler={dropHandler}
                            dragTaskStartHandler={dragTaskStartHandler}
                            dragTaskEndHandler={dragTaskEndHandler}
                            dragTaskOverHandler={dragTaskOverHandler}
                            dropTaskHandler={dropTaskHandler}
                            currentTaskColumn={currentTaskColumn}
                        />
                    );
                }
                )}
                <InputWrapperDiv
                    hidden={inputColumn}
                >
                    <InputNewColumn 
                        value={inputColumnText}
                        onChange={handleNewColumn}
                        onKeyDown={EnterClick}
                    />
                    <ButtonNewColumnMini
                        onClick={createColumn}
                    >
                    Добавить список
                    </ButtonNewColumnMini>
                </InputWrapperDiv>
                <ButtonNewColumn
                    hidden={inputColumn&&hidden}
                    onClick={showNewColumn}
                > +  Добавьте еще одну колонку </ButtonNewColumn>
            </ColumnsSpace>
            <ModalTask />
        </BoardSpace>
    );
};

const BoardSpace:StyledComponent<'div', any> = styled.div` 
background-color: #0079bf;
overflow: auto;
`;
const ButtonNewColumn:StyledComponent<'button', any> = styled.button` 
min-width: 250px;
height: 40px;
background-color: #318fc6;
border: none;
border-radius: 3px;
color: white;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
margin-top: 5px;
margin-left: 10px;
padding-left: 10px;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
justify-content: start;
align-items: center;
&:hover {
    background-color: #3d99ce;
    cursor: pointer;
  }
`;
const ButtonNewColumnMini:StyledComponent<'button', any> = styled.button` 
width: 150px;
height: 30px;
background-color: #0079bf;
border: none;
border-radius: 3px;
color: white;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
margin-left: 5px;
padding-left: 10px;
&:hover {
    background-color: #3d99ce;
    cursor: pointer;
}
`;
const InputWrapperDiv:StyledComponent<'div', any> = styled.div`
width: 277px;
height: 79px;
margin-top: 5px;
margin-left: 10px;
border-radius: 3px;
outline: 0;
border: none;
background-color: #ebecf0;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
`; 
const ColumnsSpace:StyledComponent<'div', any> = styled.div` 
height: 875px;
display: flex;
flex-direction: row;
justify-content: start;
align-items: start;
margin-left: 40px;
overflow: auto;
`;
const InputNewColumn :StyledComponent<'input', any> = styled.input` 
width: 260px;
height: 30px;
border-radius: 3px;
outline: 0;
border: 2px solid #0079bf;
margin: 5px;
`; 

export default Board;