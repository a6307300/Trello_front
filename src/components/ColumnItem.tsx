/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useState, useEffect} from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector} from 'react-redux';
import { TaskRootState } from 'typesafe-actions';
import {addTask, getTasks} from '../store/taskSlice';
import {deleteColumn, renameColumn} from '../store/columnSlice';
import TaskItem from './TaskItem';


interface IColumnItem {
    key: number,
    columnId: number,
    columnName:string,
    currentColumn: number,
    currentTask: number,
    currentTaskColumn: number,
    setCurrentColumn:any,
    setCurrentTask:any,
    dragStartHandler:any,
    dragEndHandler:any,
    dragOverHandler:any,
    dropHandler: any,
    setCurrentTaskColumn: any,
    dragTaskStartHandler:any,
    dragTaskEndHandler:any,
    dragTaskOverHandler: any,
    dropTaskHandler: any,
  }

const ColumnItem: FC<IColumnItem> = ({
    columnId, columnName, currentTask,
    setCurrentTask, dragStartHandler, dropHandler, dragEndHandler, dragOverHandler,
    setCurrentTaskColumn, dragTaskStartHandler, dragTaskEndHandler, dragTaskOverHandler, dropTaskHandler,
    currentTaskColumn }) => {

    const dispatch = useDispatch();

    const tasks = useSelector((state:TaskRootState) => state.tasks);

    useEffect(() => {
        dispatch(getTasks(localStorage.getItem('board') as string));
    }, []);

    const hidden = Boolean(localStorage.getItem('hideShared'));

    const [columnInputName, setColumnInputName] = useState<boolean>(false);
    const [inputName, setInputName] = useState<string>(columnName);
    const [newTask, setNewTask] = useState<boolean>(false);
    const [newTaskText, setNewTaskText] = useState<string>('');

    const hideNewTask = () => {
        setNewTask(false);
    };
    const showNewTask = () => {
        setNewTask(true);
    };
    const handleChangeTask = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setNewTaskText(target.value);
    };
    interface ITaskData {
        taskName: string,
      column:number,
    }
    const EnterClickTask = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            if (newTaskText) {
                const taskData: ITaskData = {
                    taskName: newTaskText,
                    column: columnId,
                };
                dispatch(addTask(taskData));
                setNewTaskText('');
            } else {
                alert('Имя новой колонки не может быть пустым');
            }
            hideNewTask();
        }
        if (e.key === 'Escape') {
            setNewTaskText('');
            hideNewTask();
        }
    };
    const hideColumnInputName = () => {
        setColumnInputName(false);
    };
    const showColumnInputName = () => {
        setColumnInputName(true);
    };
    const handleChangeName = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputName(target.value);
    };  
    const EnterClick = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            if (inputName!==''){
                const dataColumn = {
                    columnName:inputName,
                    id: columnId,
                };
                dispatch(renameColumn(dataColumn));
                hideColumnInputName();
            }
        }
        if (e.key === 'Escape') {
            setInputName(columnName);
            hideColumnInputName();
        }
    };

    const start = (e:React.DragEvent<HTMLDivElement>) => {
        //@ts-ignore
        dragStartHandler(e, columnId);
    };
    const end = (e:React.DragEvent<HTMLDivElement>) => {
        //@ts-ignore
        dragEndHandler(e, columnId);
    };
    const drop = (e:React.DragEvent<HTMLDivElement>) => {
        dropHandler(e, columnId);
    };
    const over = (e:React.DragEvent<HTMLDivElement>) => {
        dragOverHandler(e);
    };
    const delColumn = (e:React.SyntheticEvent) => {
        e.preventDefault();
        const conformationDel = confirm(`Вы уверены, что хотите удалить колонку ${columnName}?`);
        if (conformationDel) {
            dispatch(deleteColumn(columnId));
        } else {
            alert(`Операция по удалению колонки ${columnName} отменена.`);
        }
    };
    return (
        <ColumnStyledDiv
            draggable={!hidden?true:false}
            onDragStart={start}
            onDragEnd={end}
            onDragOver={over}
            onDrop={drop}
        >
            <ColumnHeaderDiv>
                <ColumnNameDiv>
                    <PreviewStyledDiv
                        hidden={columnInputName}
                        onClick={!hidden?showColumnInputName:hideColumnInputName}
                    > 
                        {columnName} 
                    </PreviewStyledDiv>
                    <PreviewStyledInput 
                        hidden={columnInputName}
                        onChange={handleChangeName}
                        onKeyDown={EnterClick}
                        value={inputName}
                    />
                </ColumnNameDiv>
                <ColumnDeleteButton
                    hidden={hidden}
                    onClick={delColumn}
                > 
            ...
                </ColumnDeleteButton>
            </ColumnHeaderDiv>
            <TasksStyledDiv
                onDragStart={(e:React.SyntheticEvent)=>e.stopPropagation()}
                onDragEnd={(e:React.SyntheticEvent)=>e.stopPropagation()}
                onDragOver={(e:React.SyntheticEvent)=>e.stopPropagation()}
                onDrop={(e:React.SyntheticEvent)=>e.stopPropagation()}
            >
                <TaskItem
                    taskId={0}
                    taskName=''
                    description=''
                    range={0}
                    column={columnName}
                    currentTask={currentTask}
                    setCurrentTask={setCurrentTask}
                    setCurrentTaskColumn={setCurrentTaskColumn}
                    dragTaskStartHandler={dragTaskStartHandler}
                    dragTaskEndHandler={dragTaskEndHandler}
                    dragTaskOverHandler={dragTaskOverHandler}
                    dropTaskHandler={dropTaskHandler}
                    currentTaskColumn={currentTaskColumn}
                />
                {Object.values(tasks).flat().map((task) => {
                    if (task.columnID==columnId) {
                        return (
                            <TaskItem 
                                key={task.id}
                                taskId={task.id}
                                taskName={task.taskName}
                                description={task.description?task.description:''}
                                range={task.range}
                                column={columnName}
                                currentTask={currentTask}
                                setCurrentTask={setCurrentTask}
                                setCurrentTaskColumn={setCurrentTaskColumn}
                                dragTaskStartHandler={dragTaskStartHandler}
                                dragTaskEndHandler={dragTaskEndHandler}
                                dragTaskOverHandler={dragTaskOverHandler}
                                dropTaskHandler={dropTaskHandler}
                                currentTaskColumn={currentTaskColumn}
                            />
                        );
                    }
                }
                )}
            </TasksStyledDiv>
            <InputNameTask
                hidden={newTask}
                value={newTaskText}
                onChange={handleChangeTask}
                onKeyDown={EnterClickTask}
            />
            <ButtonNewTask
                onClick={showNewTask}
                hidden={hidden}
            > + Добавить карточку </ButtonNewTask>
        </ColumnStyledDiv>
    );
};

const InputNameTask:StyledComponent<'input', any> = styled.input`
color: #172b4d;
font-weight: normal;
width: 245px;
min-height: 70px;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
background-color: white;
outline: 0;
border: none;
margin-left:10px;
padding-top:5px;
word-wrap : break-word;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
`;
const ColumnStyledDiv:StyledComponent<'div', any> = styled.div` 
min-width: 272px;
margin-left: 10px;
background-color: #ebecf0;
border: none;
border-radius: 3px;
display: flex;
flex-direction: column;
margin-top: 5px;
`;
const ColumnHeaderDiv:StyledComponent<'div', any> = styled.div` 
min-height: 36px;
display: flex;
flex-direction: row;
justify-content: start;
word-wrap : break-word;
`;
const PreviewStyledDiv:StyledComponent<'div', any> = styled.div`
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 800;
width: 80%;
// height: 70%;
padding-left: 10px;
padding-top: 5px;
`;
const ColumnNameDiv:StyledComponent<'div', any> = styled.div`
color: #172b4d;
font-weight: bold;
margin-left: 5px;
margin-top: 5px;
width: 90%;
// height: 80%;
display: ${(props) => ((props.hidden) ? 'none': 'block')};
margin-top: 5px;
`;
const PreviewStyledInput:StyledComponent<'input', any> = styled.input`
color: #172b4d;
font-weight: bold;
width: 100%;
// height: 80%;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 800;
background-color: transparent;
outline: 0;
border: none;
padding-left:10px;
padding-top:5px;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
`;
const TasksStyledDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
min-height: 20px;
`;
const ButtonNewTask:StyledComponent<'button', any> = styled.button` 
width: 250px;
height: 35px;
background-color: #ebecf0;
border: none;
border-radius: 3px;
color: #676767;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: normal;
margin-left: 10px;
margin-bottom: 2px;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
justify-content: start;
align-items: center;
&:hover {
    background-color: #dfdfdf;
    color: #172b4d;
  }
`;
const ColumnDeleteButton:StyledComponent<'button', any> = styled.button`
width: 10%;
height: 30px;
background-color: #ebecf0;
border: none;
border-radius: 3px;
color: #6b778c;
font-weight: bold;
padding-top: 5px;
padding-left: 7px;
margin: 3px;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
&:hover {
  background-color: #d9dadf;
}
`;

export default ColumnItem;