/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';

interface IFormChangeAva {
  hiddenAva: boolean;
  handleChangeAva: any;
  upload: any;
}

const FormChangeAva: FC<IFormChangeAva> = ({
    hiddenAva,
    handleChangeAva,
    upload,
}) => {
    return (
        <ChangeAvaStyledDiv hidden={hiddenAva}>
            <TextStyled>Добавить/изменить аватар</TextStyled>
            <input type="file" onChange={handleChangeAva} />
            <ButtonStylHelp onClick={upload}>ОK</ButtonStylHelp>
        </ChangeAvaStyledDiv>
    );
};

const ButtonStylHelp: StyledComponent<'button', any> = styled.button`
  width: 58px;
  height: 30px;
  background-color: #0052cc;
  border: none;
  border-radius: 3px;
  color: white;
  font-weight: bold;
`;
const ChangeAvaStyledDiv: StyledComponent<'div', any> = styled.div`
  display: ${(props) => (props.hidden ? 'none' : 'block')};
`;
const TextStyled: StyledComponent<'p', any> = styled.p`
  font-size: 10px;
  font-style: italic;
  color: silver;
  margin-top: 1px;
`;
export default FormChangeAva;
