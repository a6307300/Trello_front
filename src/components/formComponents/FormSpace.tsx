/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-shadow */
// eslint-disable-next-line no-unused-vars
import React, { FC} from 'react';
import styled, { StyledComponent } from 'styled-components';
// eslint-disable-next-line no-unused-vars
import InputField from './InputField';
// eslint-disable-next-line no-unused-vars
import FormChangeAva from './FormChangeAva';


interface IFormSpace {
  hiddenInputName: boolean,
  textHeader: string
  textFooter: string,
  link:string,
  color: string,
  handleChangeName?: any,
  handleChangeEmail: any,
  handleChangePassword: any,
  handleChangeAva?: any,
  inputTextName: string,
  inputTextEmail: string,
  inputTextPassword: string,
  hiddenAva: boolean
  upload?:any,
}

const FormSpace: FC<IFormSpace> = ({
    hiddenInputName,
    textHeader,
    textFooter,
    link,
    color,
    handleChangeName,
    handleChangeEmail,
    handleChangePassword,
    inputTextName,
    inputTextEmail,
    inputTextPassword,
    handleChangeAva,
    hiddenAva,
    upload
}) => (
    <RegistrationContainerStyled>
        <RegistrationFormStyled>
            <H3Styled
                color={color}
            >
                {textHeader}
            </H3Styled>
            <LabelDiv
                hidden={hiddenInputName}
            >
              Ваше имя</LabelDiv>
            <InputField
                name="name"
                placeholder="Ваше имя"
                type="text"
                hidden={hiddenInputName}
                handleChange={handleChangeName}
                inputText={inputTextName}
            />
            <LabelDiv>Ваш e-mail</LabelDiv>
            <InputField
                name="email"
                placeholder="Ваш e-mail"
                type="text"
                hidden={false}
                handleChange={handleChangeEmail}
                inputText={inputTextEmail}
            />
            <LabelDiv>Ваш пароль</LabelDiv>
            <InputField
                name="newPassword"
                placeholder="Ваш пароль"
                type="password"
                hidden={false}
                handleChange={handleChangePassword}
                inputText={inputTextPassword}
            />
            <FormChangeAva 
                hiddenAva={hiddenAva}
                handleChangeAva={handleChangeAva}
                upload={upload}
            />
        </RegistrationFormStyled>
        <a href={link}>
            <TextNotesStyled>{textFooter}</TextNotesStyled>
        </a>
    </RegistrationContainerStyled>
);
const LabelDiv: StyledComponent<'div', any> = styled.div`
  width: 400px;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  margin-left: 80px;
  color: #172b4d;
  font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
  font-size: 14px;
  font-weight: 600;
  display: ${(props) => ((props.hidden) ? 'none' : 'flex')};
`;

const RegistrationContainerStyled: StyledComponent<'div', any> = styled.div`
  width: 400px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 50px;
  padding-bottom: 40px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
`;

const RegistrationFormStyled: StyledComponent<'div', any> = styled.div`
  width: 320px;

  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  margin-top: 15px;
`;

const TextNotesStyled: StyledComponent<'p', any> = styled.p`
  font-size: 10px;
  font-style: italic;
  color: silver;
  margin-top: 10px;
`;

const H3Styled: StyledComponent<'h3', any> = styled.h3`
  font-size: 20px;
  color: ${(props) => props.color};
`;

export default FormSpace;
