/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';

interface IButtonForm {
  value: string;
  label: string;
  functionClick: any;
}

const ButtonForm: FC<IButtonForm> = ({ value, label,functionClick }) => (
    <ButtonStyled 
        type="submit" 
        value={value} 
        onClick={functionClick}>
        {label}
    </ButtonStyled>
);

const ButtonStyled: StyledComponent<'button', any, {}, never> = styled.button`
  width: 320px;
  height: 35px;
  margin-top: 60px;
  background-color: #0052cc;
  border: none;
  border-radius: 3px;
  color: white;
  font-weight: bold;
  &:hover {
    background-color: #0066ff;
  }
`;

export default ButtonForm;
