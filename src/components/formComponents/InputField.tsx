/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';

interface IInputField {
  name: string,
  placeholder: string,
  type: string,
  hidden: boolean,
  handleChange:any,
  inputText:string,
}

const InputField: FC<IInputField> = ({
    name, placeholder, type, hidden, handleChange, inputText
}) => (
    <InputStyled
        name={name}
        type={type}
        placeholder={placeholder}
        hidden={hidden}
        onChange={handleChange}
        defaultValue={inputText}
    />
);

const InputStyled: StyledComponent<'input', any>  = styled.input`
  width: 312px;
  min-height: 35px;
  margin-bottom: 10px;
  border-radius: 3px;
  border: 2px solid #dfe1e6;
  display: ${(props) => ((props.hidden) ? 'none' : 'block')};
  color: #172b4d;
  font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
  font-size: 14px;
  font-weight: 400;
  word-wrap : break-word;
  &:hover {
    background-color: #dfe1e0;
  }
  &:focus {
    outline-color: #0052cc;
  }
`;

export default InputField;
