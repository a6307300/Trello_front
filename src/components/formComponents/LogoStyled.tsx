/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC } from 'react';
import styled, {StyledComponent} from 'styled-components';

const LogoTrello: FC = () => <LogoStyled src="../images/Trello2.png" />;

const LogoStyled:StyledComponent<'img', any> = styled.img`
  margin-top: 70px;
  height: 50px;
`;

export default LogoTrello;
