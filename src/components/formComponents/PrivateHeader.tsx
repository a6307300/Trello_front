/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, FC } from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import {MyRootState} from 'typesafe-actions';
import { renameBoard } from '../../store/boardSlice';

interface IPrivateHeader {
  inputTextName: string,
  sharedBoard: any;
}

const PrivateHeader: FC<IPrivateHeader> = ({sharedBoard}) => {

    const dispatch = useDispatch();

    const hidden = Boolean(localStorage.getItem('hideShared'));
    const hiddenShare = Boolean(localStorage.getItem('hideShared'));

    const [boardInputName, setBoardInputName] = useState<boolean>(false);
    const [inputName, setInputName] = useState<string>(localStorage.getItem('boardName') as string);

    const handleChangeName = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputName(target.value);
    };  

    const hideBoardInputName = () => {
        setBoardInputName(false);
    };

    const showBoardInputName = () => {
        setBoardInputName(true);
    };

    const EnterClick = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            if (inputName!==''){
                const dataBoard = {
                    boardName:inputName,
                    id: localStorage.getItem('board') as string,
                };
                dispatch(renameBoard((dataBoard)));
                localStorage.setItem('boardName',inputName);
                hideBoardInputName();
            }
        }
        if (e.key === 'Escape') {
            setInputName(localStorage.getItem('board') as string);
            hideBoardInputName();
        }
      
    };

    const [avatar, setAvatar] = useState<string>(localStorage.getItem('authorAva') as string);
    const avatar2 = useSelector<MyRootState>(state => state.user);
    useEffect(() => {
        setAvatar(localStorage.getItem('authorAva') as string);
    }, [avatar2]);

    return (
        <BoardHeaderWrapperDiv>
            <BoardHeaderDiv>
                <BoardDiv>
                    <BoardImg src='images/col.png' />
                Доска
                </BoardDiv>
                <BoardNameDiv
                    hidden={boardInputName}
                    onClick={!hidden?showBoardInputName:hideBoardInputName}
                >
                    {localStorage.getItem('boardName')}
                </BoardNameDiv>
                <BoardNameInput 
                    hidden={boardInputName}
                    onChange={handleChangeName}
                    onKeyDown={EnterClick}
                    value ={inputName}
                />
                <LineDiv />
                <BoardNameOwnerDiv>
                    {localStorage.getItem('authorName')}
                </BoardNameOwnerDiv>
                <LineDiv />
                <AvaDiv>
                    <AvaImg src={avatar} />
                </AvaDiv>
                <ShareDiv
                    hidden={hiddenShare}
                    onClick={sharedBoard}
                >
                Пригласить
                </ShareDiv>
            </BoardHeaderDiv>
        </BoardHeaderWrapperDiv>
    );
};

const BoardHeaderWrapperDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
  height: 48px;
  display: flex;
  flex-direction: row;
  background-color: #0079bf;
`;
const BoardHeaderDiv:StyledComponent<'div', any> = styled.div` 
  width: 96.5%;
  height: 40px;
  display: flex;
  justify-content: start;
  flex-direction: row;
  margin: 8px 4px 4px 48px;
`;

const BoardDiv:StyledComponent<'div', any> = styled.div` 
  width: 110px;
  height: 32px;
  background-color: #318fc6;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
  font-size: 14px;
  font-weight: 400;
  border-radius: 3px;
  display: flex;
  justify-content: start;
  align-items: center;
  color: white;
  margin-right: 8px;
  &:hover {
    background-color: #3d99ce;
    cursor: pointer;
  }
`;

const BoardImg:StyledComponent<'img', any> = styled.img` 
height: 15px;
width: 15px;
padding: 6px;
`;

const BoardNameDiv:StyledComponent<'div', any> = styled.div` 
min-width: 30px;
height: 32px;
background-color: #318fc6;
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
font-size: 18px;
font-weight: 800;
border-radius: 3px;
justify-content: center;
align-items: center;
color: white;
padding: 0 5px;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
&:hover {
  background-color: #3d99ce;
  cursor: pointer;
}
`;

const BoardNameInput:StyledComponent<'input', any> = styled.input` 
height: 32px;
min-width: 30px;
background-color: #318fc6;
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
font-size: 14px;
font-weight: 800;
border-radius: 3px;
border: none;
outline: 0;
display: flex;
padding: 0 5px;
justify-content: start;
align-items: center;
color: white;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
`;

const LineDiv:StyledComponent<'div', any> = styled.div` 
  width: 1px;
  height: 16px;
  background-color: #318fc6;
  margin: 10px 8px 12px 8px;
`;

const BoardNameOwnerDiv:StyledComponent<'div', any> = styled.div` 
min-width: 20px;
height: 32px;
background-color: #318fc6;
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
font-size: 14px;
font-weight: 400;
border-radius: 3px;
display: flex;
justify-content: start;
padding: 0 5px;
align-items: center;
color: white;
&:hover {
  background-color: #3d99ce;
  cursor: pointer;
}
`;

const AvaDiv:StyledComponent<'div', any> = styled.div` 
  width: 28px;
  height: 28px;
  margin-right: 7px;
  &:hover {
    opacity: 70%;
    cursor: pointer;
  }
`;
const AvaImg:StyledComponent<'img', any> = styled.img` 
  width: 28px;
  height: 28px;
  border-radius: 50%;
  object-fit:cover;
`;

const ShareDiv:StyledComponent<'div', any> = styled.div` 
width: 100px;
height: 32px;
background-color: #318fc6;
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
font-size: 14px;
font-weight: 400;
border-radius: 3px;
display: flex;
justify-content: center;
align-items: center;
color: white;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
&:hover {
  background-color: #3d99ce;
  cursor: pointer;
}
`;


export default PrivateHeader;
