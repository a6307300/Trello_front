/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, FC } from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector} from 'react-redux';
import { MentionsInput, Mention, MentionItem } from 'react-mentions';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import DoneOutlineIcon from '@mui/icons-material/DoneOutline';
import { deleteTask, describeTask, rangeTask, renameTask } from '../store/taskSlice';
import { CommentRootState, ModalRootState, UsersRootState } from 'typesafe-actions';
import CommentItem from './CommentItem';
import { addComment, getComments } from '../store/commentSlice';
import { setActState, setDescript, setRange, setTaskName } from '../store/taskModalSlice';

const ModalTask: FC= () => {

    const dispatch = useDispatch();
    
    const comments = useSelector((state:CommentRootState) => state.comments);

    const usersList = useSelector((state:UsersRootState) => state.usersList);
    const ottt = JSON.stringify(usersList);
    const usersL =JSON.parse(ottt).usersList;


    const taskModal = useSelector((state:ModalRootState) => state.taskModal);
    const ot = JSON.stringify(taskModal);
    const modal =JSON.parse(ot).taskModal;
    
    const [changeDescription, sethangeDescription] = useState<boolean>(false);
    const [changeName, sethangeName] = useState<boolean>(false);
    const [taskRange, setTaskRange] = useState<boolean>(false);
    const [comment, setComment] = useState<boolean>(false);
    const [inputTextName, setInputTextName] = useState<string>(modal.taskName);
    const [inputDescription, setInputDescription] = useState<string>(modal.description);
    const [inputRange, setInputRange] = useState<string>(modal.range.toString());
    const [inputComment, setInputComment] = useState<string>(' ');

    useEffect(() => {
        dispatch(getComments(localStorage.getItem('board') as string));
    }, [modal.act]);

    const hidden = Boolean(localStorage.getItem('hideShared'));
    const hiddenRange = Boolean(modal.range);

    const deactivateModalTask = (e:React.SyntheticEvent) => {
        e.stopPropagation();
        dispatch(setActState(false));
    };
    const showDescription = () => {
        sethangeDescription(true);
    };
    const hideDescription = () => {
        sethangeDescription(false);
    };
    const showName = () => {
        sethangeName(true);
    };
    const hideName = () => {
        sethangeName(false);
    };
    const showRange = () => {
        setTaskRange(true);
    };
    const hideRange = () => {
        setTaskRange(false);
    };
    const showInputComment = () => {
        setComment(true);
    };
    const hideInputComment = () => {
        setComment(false);
    };
    
    type OnChangeHandlerFunc = (event: {
        target: {
            value: string;
        };
    }, newValue: string, newPlainTextValue: string, mentions: MentionItem[]) => void

    const handleInputComment:OnChangeHandlerFunc = (e) => {
        const target = e.target as HTMLInputElement;
        setInputComment(target.value);
    };  
    const handleChangeName = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextName(target.value);
    };  
    const handleDescription = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputDescription(target.value);
    };
    const handleRate = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputRange(target.value);
    };
    const EnterClick = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            if (inputTextName!==''){
                if (inputTextName.length<256) {
                    const dataTask = {
                        taskName:inputTextName,
                        id: modal.taskId,
                    };
                    dispatch(renameTask(dataTask));
                    dispatch(setTaskName(inputTextName));
                    hideName();
                } else alert ('Слишком длинное имя. Сократите его до 255 символов.');
            }
        }
        if (e.key === 'Escape') {
            alert('escape');
            setInputTextName(modal.taskName);
            hideName();
        }
    };
    const completeTask = () => {
        if (+modal.range>0) {
            const dataTask = {
                range:'0',
                id: modal.taskId,
            };
            dispatch(rangeTask(dataTask));
            dispatch(setRange(0));
        } else {
            const dataTask = {
                range:'1',
                id: modal.taskId,
            };
            dispatch(rangeTask(dataTask));
        }
    };
    const EnterClickRange = (e: React.KeyboardEvent) => {
        if (e.key == 'Enter') {
            if (inputRange.toString()!==''){
                if (+inputRange >= 0 && +inputRange <= 5) {
                    if ((+inputRange ^ 0) === +inputRange) {
                        const dataTask = {
                            range:inputRange,
                            id: modal.taskId,
                        };
                        dispatch(rangeTask(dataTask));
                        dispatch(setRange(inputRange));
                        hideRange();
                    } else alert ('Ранг задачи должен быть предстален целым числом от 0(выполненная задача) до 5(задача максимальной важности)');
                } else alert ('Ранг задачи может варьироваться от 0(выполненная задача) до 5 (максимальная важность)');
            }
        }
        if (e.key == 'Escape') {
            setInputRange(modal.range.toString());
            hideRange();
        }
    };
    const setDescription = (e: React.SyntheticEvent) => {
        e.preventDefault();
        const dataTask = {
            description:inputDescription,
            id: modal.taskId,
        };
        dispatch(describeTask(dataTask));
        dispatch(setDescript(inputDescription));
        hideDescription();
        setInputDescription(inputDescription);
    };
    const delTask = (e: React.SyntheticEvent) => {
        e.preventDefault();
        dispatch(deleteTask(modal.taskId));
        dispatch(setActState(false));
    };
    const EnterClickDescription = (e: React.KeyboardEvent) => {
        if (e.key == 'Enter') {
            if (inputComment!==''){
                setDescription(e);
            }
        }
        if (e.key == 'Escape') {
            setInputDescription(modal.description);
            hideDescription();
        }
    };
    const addNewComment = (e: React.SyntheticEvent) => {
        e.preventDefault();
        if (inputComment!==''){
            if(inputComment.length<255) {
                const dataComment = {
                    commentText:inputComment,
                    task: modal.taskId,
                    authorAva: localStorage.getItem('ava') as string,
                    authorName: localStorage.getItem('name') as string,
                };
                dispatch(addComment(dataComment));
                setInputComment(' ');
                hideInputComment();
            } else alert ('Слишком длинный комментарий. Сократите его до 255 символов.');
        }
    };
    const EnterClickComment = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            addNewComment(e);
        }
        if (e.key === 'Escape') {
            alert('escape');
            setInputComment(' ');
            hideInputComment();
        }
    };
    return(
        <DisplaySpaceDiv
            hidden={modal.act}
            onClick={deactivateModalTask}
        >
            <TaskWindowDiv
                onClick={(e:React.SyntheticEvent)=>e.stopPropagation()}
            >
                <TaskNameSpace>
                    <TaskIconDiv
                        onClick={completeTask}
                    >
                        <CheckCircleOutlineIcon />
                    </TaskIconDiv>
                    <TaskColumnSpace>
                        <TaskNameDiv
                            hidden={changeName}
                            onClick={!hidden?showName:hideName}
                        >
                            {modal.taskName}
                        </TaskNameDiv>
                        <TaskNameInput
                            hidden={changeName}
                            value={inputTextName}
                            onKeyPress={EnterClick}
                            onChange={handleChangeName}
                        />

                        <TaskColumnDiv>
                      в колонке {modal.column}
                        </TaskColumnDiv>
                    </TaskColumnSpace>
                    <TaskIconXDiv
                        onClick={deactivateModalTask}
                    >
                      x
                    </TaskIconXDiv>
                </TaskNameSpace>

                <TaskDescriptionSpace
                    hidden={changeDescription}
                >
                    <TaskDescriptionHeader>
                        <TaskIconDiv
                            onClick={delTask}
                        >
                            <HighlightOffIcon/>
                        </TaskIconDiv>
                        <Description> Описание:</Description>
                        <DescriptionButton
                            onClick={showDescription}
                            hidden={hidden}
                        >
                          Изменить
                        </DescriptionButton>
                        <DescriptionRange> Рейтинг </DescriptionRange>
                    </TaskDescriptionHeader>
                    <TaskDescriptionBody>
                        <TaskDiv>
                            {modal.description}
                        </TaskDiv>
                        <RateSpace>
                            <TaskRateDiv
                                hidden={taskRange}
                                onClick={showRange}
                            >
                                {hiddenRange?modal.range:<DoneOutlineIcon/>}
                            </TaskRateDiv>
                            <TaskRateInput
                                hidden={taskRange}
                                onChange={handleRate}
                                value={inputRange}
                                onKeyPress={EnterClickRange}
                            />
                        </RateSpace>
                    </TaskDescriptionBody>
                </TaskDescriptionSpace>
                <TaskDescriptionSpace2
                    hidden={changeDescription}
                >
                    <TaskDescriptionHeader>
                        <TaskIconDiv
                            onClick={delTask}
                        >
                            <HighlightOffIcon/>
                        </TaskIconDiv>
                        <Description> Описание:</Description>
                        <TaskDescriptionDiv />
                        <DescriptionRange> Рейтинг </DescriptionRange>
                    </TaskDescriptionHeader>
                    <TaskDescriptionBody>
                        <TaskChangeDiv>
                            <DescriptionInput 
                                type='text'
                                onChange={handleDescription}
                                onKeyDown={EnterClickDescription}
                                value={inputDescription}
                            />
                            <DescriptionButtons>
                                <DescriptionSaveButton
                                    onClick={setDescription}
                                >
                                  Сохранить
                                </DescriptionSaveButton>
                                <DescriptionSaveButton
                                    onClick={hideDescription}
                                >
                                  Х
                                </DescriptionSaveButton>
                            </DescriptionButtons>
                        </TaskChangeDiv>
                        <RateSpace>
                            <TaskRateDiv
                                hidden={taskRange}
                                onClick={showRange}
                            >
                                {modal.range}
                            </TaskRateDiv>
                            <TaskRateInput
                                hidden={taskRange}
                                onChange={handleRate}
                                onKeyPress={EnterClickRange}
                            />
                        </RateSpace>
                    </TaskDescriptionBody>
                </TaskDescriptionSpace2>
                <TaskCommentSpace>
                    <TaskCommentHeader>
                        <TaskIconDiv>
                            <AvaStyled src={localStorage.getItem('ava') as string} />
                        </TaskIconDiv>
                        <NewCommentDiv>
                            <Comments
                                hidden={comment}
                                onClick={showInputComment}
                            > Напишите комментарий...</Comments>
                            <CommentsInputDiv
                                hidden={comment}
                            >
                                <MentionsInput style={{ color: '#172b4d',fontSize: '14px', 
                                    border: 'none', outline:'none', width: '100%', minHeight: '50px'}} 
                                value={inputComment} onChange={handleInputComment} onKeyDown={EnterClickComment}>
                                    <Mention
                                        trigger="@"
                                        data={usersL}
                                    />
                                </MentionsInput>
                                <CommentSaveButton
                                    onClick={addNewComment}
                                >
                                Сохранить
                                </CommentSaveButton>
                            </CommentsInputDiv>
                        </NewCommentDiv>
                    </TaskCommentHeader>
                    <TaskCommentsDiv>
                        {Object.values(comments).flat().map((comment) => {
                            if (comment.taskID==modal.taskId) {
                                return (
                                    <CommentItem 
                                        key={comment.id}
                                        commentId={comment.id}
                                        comment={comment.comment}
                                        userID={comment.userID}
                                        taskID={comment.taskID}
                                        authorName={comment.author.fullName}
                                        authorAva={comment.author.avatar}
                                    />                                   
                                );
                            }
                        }
                        )}
                    </TaskCommentsDiv>
                </TaskCommentSpace>
            </TaskWindowDiv>
        </DisplaySpaceDiv>
    );
};
const AvaStyled:StyledComponent<'img', any> = styled.img` 
  height: 35px;
  width: 35px;
  margin-top: 15px;
  border-radius: 50%;
  object-fit: cover;
`;
const DisplaySpaceDiv:StyledComponent<'div', any> = styled.div` 
  width: 100vw;
  height: 100vh;
  z-index: 9999;
  background-color: rgba(0,0,0,0.4);
  position: fixed;
  opacity: 95%;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: ${(props) => ((props.hidden) ? 'scale(1)': 'scale(0)')};
`;
const TaskWindowDiv:StyledComponent<'div', any> = styled.div` 
  width: 600px;
  background-color: rgba(240,240,240,1);
  border: none;
  border-radius: 3px;
  color: #172b4d;
  font-weight: bold;
  display: flex;
  flex-direction: column;
  align-items: center;
  opacity: 1;
  cursor: default;
`;
const TaskNameSpace:StyledComponent<'div', any> = styled.div` 
  width: 100%;
  min-height: 50px;
  display:flex;
  flex-direction: row;
  justify-content: center;
`;
const TaskIconDiv:StyledComponent<'div', any> = styled.div` 
height: 30px;
width: 60px;
display: flex;
flex-direction: row;
  justify-content: center;
  align-items: center;
`;
const TaskIconXDiv:StyledComponent<'div', any> = styled.div` 
min-height: 50px;
width: 20px;
font-size: 18px;
font-weight: normal;
color: grey;
text-align: end;
margin-right: 5px;
&:hover {
  cursor: pointer;
}`;
const TaskColumnSpace:StyledComponent<'div', any> = styled.div` 
min-height: 50px;
width: 100%;
display:flex;
flex-direction: column;
`;
const TaskNameDiv:StyledComponent<'div', any> = styled.div` 
min-height: 30px;
width: 400px;
font-size: 24px;
display: ${(props) => ((props.hidden) ? 'none': 'block')};
word-wrap : break-word;
`;
const TaskNameInput:StyledComponent<'input', any> = styled.input` 
min-height: 30px;
width: 90%;
font-size: 24px;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
font-weight: bold;
background-color: rgba(240,240,240,1); 
`;
const TaskColumnDiv:StyledComponent<'div', any> = styled.div` 
min-height: 20px;
width: 100%;
color: grey;
font-size: 14px;
font-style: italic;
font-weight: normal;
`;
const TaskDescriptionSpace:StyledComponent<'div', any> = styled.div` 
  margin-top: 10px;
  width: 100%;
  display:flex;
  flex-direction: column;
  display: ${(props) => ((props.hidden) ? 'none': 'block')};
`;
const TaskDescriptionSpace2:StyledComponent<'div', any> = styled.div` 
  margin-top: 10px;
  width: 100%;
  display:flex;
  flex-direction: column;
  display: ${(props) => ((props.hidden) ? 'block': 'none')};
`;
const TaskDescriptionHeader:StyledComponent<'div', any> = styled.div` 
  width: 600px;
  height: 30px;
  display:flex;
  flex-direction: row;
  align-items: center;
`;
const Description:StyledComponent<'div', any> = styled.div` 
width: 100px;
`;
const DescriptionRange:StyledComponent<'div', any> = styled.div` 
width: 100px;
margin-left: 300px;
display: flex;
justify-content: end;
`;
const DescriptionButton:StyledComponent<'button', any> = styled.button` 
width: 100px;
height: 20px;
background-color: #eaecef;
border: none;
border-radius: 3px;
color: #172b4d;
display: ${(props) => ((props.hidden) ? 'none': 'inline')};
&:hover {
  background-color: #b0b7c2;
}`;
const TaskDescriptionDiv:StyledComponent<'div', any> = styled.div` 
width: 100px;
height: 20px;
`;
const TaskDiv:StyledComponent<'div', any> = styled.div` 
width: 78%;
min-height: 56px;
margin-left: 50px;
padding: 8px 8px 8px 12px;
word-wrap : break-word;
background-color: #eaecef;
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
`;
const TaskChangeDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
margin-left: 50px;
`;
const DescriptionInput:StyledComponent<'input', any> = styled.input` 
width: 480px;
height: 80px;
border: solid;
border-radius: 5px;
border-color: #0052cc;
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
`;
const DescriptionButtons:StyledComponent<'div', any> = styled.div` 
width: 22%;
height: 20px;
display: flex;
justify-content: space-between;
margin-top: 5px;
`;
const DescriptionSaveButton:StyledComponent<'button', any> = styled.button` 
background-color: #0052cc;
border: none;
border-radius: 3px;
color: white;
`;
const TaskDescriptionBody:StyledComponent<'div', any> = styled.div` 
display: flex;
flex-direction: row;
font-size: 18px;
font-weight: normal;
color: grey;
`;
const RateSpace:StyledComponent<'div', any> = styled.div` 
width: 10%;
font-size: 30px;
text-align: center;
color: #172b4dy;
`;
const TaskRateDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
font-size: 30px;
text-align: center;
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
display: ${(props) => ((props.hidden) ? 'none': 'block')};
`;
const TaskRateInput:StyledComponent<'input', any> = styled.input` 
width: 35px;
font-size: 30px;
text-align: center;
color: #172b4d;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
background-color: #0052cc;
border: none;
border-radius: 3px;
color: white;
`;
const TaskCommentSpace:StyledComponent<'div', any> = styled.div`
margin-top: 30px;
width: 100%;
display:flex;
flex-direction: column;
`;
const TaskCommentHeader:StyledComponent<'div', any> = styled.div` 
width: 542px;
display:flex;
flex-direction: row;
margin-bottom: 15px;
`;
const NewCommentDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
`;
const Comments:StyledComponent<'div', any> = styled.div` 
width: 100%;
height: 36px;
margin-top: 3px;
margin-bottom: 3px;
border-radius: 3px;
border: 1px solid grey;
font-weight: normal;
color: grey;
background-color: white;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
align-items: center;
`;
const CommentsInputDiv:StyledComponent<'div', any> = styled.div` 
width: 99%;
min-height: 80px;
margin-top: 3px;
margin-bottom: 3px;
border-radius: 3px;
box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 0.2);
font-weight: normal;
color: grey;
display: flex;
flex-direction: column;
background-color: white;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
`;

// const CommentsInput:StyledComponent<'input', any> = styled.input` 
// width: 98%;
// min-height: 50px;
// border: none;
// outline: none;
// display: flex;
// flex-flow: row wrap;
// align-items: start;
// color: #172b4d;
//     font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
//     font-size: 14px;
//     font-weight: 400;
// `;

const CommentSaveButton:StyledComponent<'button', any> = styled.button` 
width: 100px;
height: 25px;
border: none;
background-color: silver;
border-radius: 3px;
margin-left: 1px;
opacity: 50%;
`;
const TaskCommentsDiv:StyledComponent<'div', any> = styled.div` 
  width: 97%;
  min-height: 30px;
`;
export default ModalTask;