/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useState, useEffect } from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { MyRootState } from 'typesafe-actions';
import { addBoard } from '../store/boardSlice';

const Header:FC = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [avatar, setAvatar] = useState<string>(localStorage.getItem('ava') as string);
    const avatar2 = useSelector<MyRootState>(state => state.user);
    
    useEffect(() => {
        setAvatar(localStorage.getItem('ava') as string);
    }, [avatar2]);

    const hideSharedBoards = () => {
        localStorage.setItem('sharedBoards', '');
        history.push('/');
    };
    const showSharedBoards = () => {
        localStorage.setItem('sharedBoards', '1');
        history.push('/');  
    };
    interface IBoardData {
      boardName: string,
    }
    const createBoard = (e:React.SyntheticEvent) => {
        e.preventDefault();
        const inputBoardName = prompt('Введите имя новой доски','');
        if (inputBoardName) {
            if (inputBoardName.length<20) {
                const boardData: IBoardData = {
                    boardName: inputBoardName
                };
                dispatch(addBoard(boardData));
                hideSharedBoards();
            } else alert('Слишком длинное имя для Вашей доски, введите имя длиной до 20 символов');
        } else {
            alert('Имя новой доски не может быть пустым');
        }
    };
    return (
        <HeaderWrapDiv>
            <HeaderDiv>
                <IconDiv>
                    <ImgIconDiv src='images/icon00.png'/>
                </IconDiv>
                <IconTrello>
                </IconTrello>
                <CreateButtonDiv>
                    <CreateButton
                        onClick={hideSharedBoards}
                    >
                        Доски
                    </CreateButton>
                    <CreateButton
                        onClick={showSharedBoards}
                    >
                        Shared
                    </CreateButton>
                    <CreateButton
                        onClick={createBoard}
                    >
                        Создать
                    </CreateButton>
                </CreateButtonDiv>
                <AvaDiv
                    onClick = {() => history.push('/profile')} />
                <AvaImg src={avatar} 
                    onClick = {() => history.push('/profile')}
                />
            </HeaderDiv>
        </HeaderWrapDiv>
    );
};


const HeaderWrapDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
height: 44px;
background-color: #0C3953;
display: flex;
`;
const HeaderDiv:StyledComponent<'div', any> = styled.div` 
width: 99%;
height: 32px;
margin: 6px 0.5% 6px 0.5%;
display: flex;
justify-content: start;
flex-direction: row;
`;
const IconDiv:StyledComponent<'div', any> = styled.div` 
width: 32px;
height: 32px;
display: flex;
justify-content: start;
align-items: center;
`;
const ImgIconDiv:StyledComponent<'img', any> = styled.img` 
width: 15px;
height: 15px;

`;
const IconTrello:StyledComponent<'div', any> = styled.div` 
width: 80px;
height: 31px;
background-image: url(https://a.trellocdn.com/prgb/dist/images/header-logo-spirit-loading.87e1af770a49ce8e84e3.gif);
background-repeat: no-repeat;
background-size: contain;
background-position: center;
`;
const CreateButtonDiv:StyledComponent<'div', any> = styled.div` 
// width: 240px;
height: 32px;
margin-left: 12px;
display: flex;
`;
const CreateButton:StyledComponent<'button', any> = styled.button` 
width: 60px;
margin-left: 12px;
height: 32px;
color: white;
background-color: #00324e;
border: none;
border-radius: 3px;
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Noto Sans', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', sans-serif;
&:hover {
  background-color: #3d6f8c;
};
&:active {
  background-color: #3d6f8c;
};
`;
const AvaDiv:StyledComponent<'div', any> = styled.div` 
width: 35px;
height: 32px;
margin-left: 78%;
`;
const AvaImg:StyledComponent<'img', any> = styled.img` 
width: 32px;
height: 32px;
border-radius: 50%;
object-fit:cover;
`;

export default Header;
