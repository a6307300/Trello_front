/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC} from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch} from 'react-redux';
import { useHistory } from 'react-router-dom';
import {deleteBoard} from '../store/boardSlice';
interface IBoardPreview {
    key: number,
    boardName:string,
    boardId:number,
    hidden:boolean,
    authorName: string,
    authorAva: string,
  }

const BoardPreview: FC<IBoardPreview> = ({ boardName, boardId, authorName, authorAva, hidden}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const delBoard = (e:React.SyntheticEvent) => {
        e.preventDefault();
        const conformationDel = confirm(`Вы уверены, что хотите удалить доску ${boardName}?`);
        if (conformationDel) {
            localStorage.setItem('boardId',boardId.toString());
            dispatch(deleteBoard());
        } else {
            alert(`Операция по удалению доски ${boardName} отменена.`);
        }
    };
    return (
        <ContainerPreviewDiv>
            <PreviewStyledDiv
                onClick = { () => {
                    localStorage.setItem('board',boardId.toString());
                    localStorage.setItem('boardName',boardName);
                    localStorage.setItem('authorName',authorName);
                    localStorage.setItem('authorAva',authorAva);
                    const hideShared = hidden?'1':'';
                    localStorage.setItem('hideShared',hideShared);
                    history.push('/board');
                }
                }
            >
                {boardName}
            </PreviewStyledDiv>
            <PreviewDeleteButton
                hidden={hidden}
                onClick= {delBoard}
            >
              ...
            </PreviewDeleteButton>
        </ContainerPreviewDiv>
    );
};

const ContainerPreviewDiv:StyledComponent<'div', any> = styled.div` 
width: 194px;
min-height: 96px;
margin: 20px 10px 10px 0px;
background-color: #0079bf;
border: none;
border-radius: 3px;
display: flex;
flex-direction: column;
`;

const PreviewStyledDiv:StyledComponent<'div', any> = styled.div` 
width: 180px;
color: white;
font-weight: bold;
padding-left: 10px;
padding-top: 10px;
display: flex;
align-content: start;
justify-content: start;
word-wrap : break-word;
min-height: 70px;
`;

const PreviewDeleteButton= styled.button`
background-color: #0079bf;
color: white;
font-weight: bold;
font-size: 16px;
height: 20px;
margin-left: 165px;
margin-top: 1px;
border: none;
border-radius: 3px;
visibility: ${(props) => ((props.hidden) ? 'hidden': 'visible')};
`;

export default BoardPreview;