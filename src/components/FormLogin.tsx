/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect, FC } from 'react';
import styled, { StyledComponent } from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginUser } from '../store/userSlice';
import FormSpace from './formComponents/FormSpace';
import LogoTrello from './formComponents/LogoStyled';
import ButtonForm from './formComponents/ButtonForm';

const FormLogin: FC = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        localStorage.clear();
    }, []);
    const [inputTextEmail, setInputTextEmail] = useState<string>('');
    const [inputTextPassword, setInputTextPassword] = useState<string>('');

    const handleChangeEmail = (e:React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextEmail(target.value);
    };
    const handleChangePassword = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextPassword(target.value);
    };
    interface IDataUserShort {
      email: string,
      password: string,
    }
    const getToken = async() => {
        if (inputTextEmail&&inputTextPassword) {
            const aboutUser: IDataUserShort = {
                email: inputTextEmail,
                password: inputTextPassword,
            };
            await dispatch(loginUser(aboutUser));
            setInputTextEmail('');
            await setInputTextPassword('');
            history.push('/');
        } else 
        {
            alert('Заполните оба поля формы авторизации');
        }
    };
    return(
        <CommonSpaceStyled>
            <LogoTrello />
            <FormSpace
                hiddenInputName={true}
                textHeader="Авторизуйтесь для работы с trello"
                textFooter="У меня нет аккунта. Регистрация"
                link="/signup"
                color="#5e6c84"
                handleChangeEmail={handleChangeEmail}
                handleChangePassword={handleChangePassword}
                inputTextName=''
                inputTextEmail={inputTextEmail}
                inputTextPassword={inputTextPassword}
                hiddenAva={true}
            />
            <ButtonForm
                value='login'
                label="Авторизоваться"
                functionClick={getToken}
            />
            {/* <a href='/'>Профиль</a> */}
        </CommonSpaceStyled>
    );
};
const CommonSpaceStyled: StyledComponent<'div', any>  = styled.div`
width:100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;

export default FormLogin;
