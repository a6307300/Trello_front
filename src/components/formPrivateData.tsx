/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, FC } from 'react';
import styled, {StyledComponent} from 'styled-components';
import {MyRootState} from 'typesafe-actions';
import { useDispatch, useSelector } from 'react-redux';
import FormSpace from './formComponents/FormSpace';
import Header from './Header';
import {modifyUser, uploadAvatar} from '../store/userSlice';
import ButtonForm from './formComponents/ButtonForm';

const FormPrivateData: FC = () => {

    const dispatch = useDispatch();

    const [avatar, setAvatar] = useState<string>(localStorage.getItem('ava') as string);
    const avatar2 = useSelector<MyRootState>(state => state.user);

    useEffect(() => {
        setAvatar(localStorage.getItem('ava') as string);
    }, [avatar2]);
 
    const [inputTextEmail, setInputTextEmail] = useState<string>(
    localStorage.getItem('email') as string
    );
    const [inputTextName, setInputTextName] = useState<string>(
    localStorage.getItem('name') as string
    );
    const [inputTextPassword, setInputTextPassword] = useState<string>('');
    const [img, setImg] = useState<any>(null);

    const handleChangeEmail = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextEmail(target.value);
    };
    const handleChangePassword = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextPassword(target.value);
    };
    const handleChangeName = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextName(target.value);
    };
    const handleChangeAva = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        { /* @ts-ignore */ }
        setImg(target.files[0]);
    };
    const user = useSelector<MyRootState>(state => state.user);
    useEffect(() => {
        setInputTextEmail(localStorage.getItem('email') as string);
        setInputTextName(localStorage.getItem('name') as string);
    }, [user]);
  interface IDataUser {
    fullName: string;
    email: string;
    password: string;
    oldPassword: string;
  }
  const modify = (e: React.SyntheticEvent) => {
      e.preventDefault();
      const veryfication = prompt('Ваш прежний пароль для подтверждения');
      if(veryfication) {
          if (inputTextName&&inputTextEmail){
              if (inputTextName.length<21) {
                  const aboutUser: IDataUser = {
                      fullName: inputTextName,
                      email: inputTextEmail,
                      password: inputTextPassword,
                      oldPassword: veryfication as string,
                  };
                  dispatch(modifyUser(aboutUser));
                  setInputTextPassword('');
              } else alert('Имя пользователя должно быть короче 20 символов');
          } else {
              alert ('Вы не указали имя пользователя или e-mail');
              setInputTextName(localStorage.getItem('name') as string);
              setInputTextEmail(localStorage.getItem('email') as string);
              setInputTextPassword('');
          }
      } else {
          alert('Пароль был введен неверно');
          setInputTextPassword('');
      }
  };
  const upload = (e: React.SyntheticEvent) => {
      e.preventDefault();
      const data = new FormData();
      data.append('file', img);
      dispatch(uploadAvatar(data));
  };
  return (
      <CommonSpaceStyled>
          <Header/>
          <AvaNameEmailDiv>
              <AvaDiv>
                  <AvaImg src={avatar} />
              </AvaDiv>
              <NameDiv>
                  {localStorage.getItem('name') as string}
              </NameDiv>
              <EmailDiv>
                  {localStorage.getItem('email') as string}
              </EmailDiv>
          </AvaNameEmailDiv>
          <FormSpace
              hiddenInputName={false}
              textHeader="Управление персональными данными"
              textFooter="Сменить пользователя"
              link="/login"
              color="#172b4d"
              handleChangeName={handleChangeName}
              handleChangeEmail={handleChangeEmail}
              handleChangePassword={handleChangePassword}
              handleChangeAva={handleChangeAva}
              hiddenAva={false}
              inputTextName={inputTextName}
              inputTextEmail={inputTextEmail}
              inputTextPassword={inputTextPassword}
              upload={upload}
          />
          <ButtonForm
              value="update"
              label="Сохранить изменения"
              functionClick={modify}
          />
          {/* <ConformationDiv>
              <p> Введите действующий пароль для подтверждения:</p>
              <ConformationInput 
                  type="password"
                  onChange={handlePassword}
              />
          </ConformationDiv> */}
      </CommonSpaceStyled>
  );
};

// const ConformationDiv:StyledComponent<'div', any> = styled.div` 
// width: 300px;
// position: fixed;
// height: 100px;
// display: flex;
// flex-direction: column;
// justify-content: center;
// align-items: center;
// background-color: white;
// box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
// padding-left: 20px;
// `;

// const ConformationInput:StyledComponent<'input', any> = styled.input` 
// width: 180px;
// height: 20px;
// background-color: yellow;
// outline: none;
// border: none;
// border-radius: 3px;
// `;



const CommonSpaceStyled:StyledComponent<'div', any> = styled.div` 
width: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;
const AvaNameEmailDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
height: 152px;
background-color: #f4f5f7;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
`;
const AvaDiv:StyledComponent<'div', any> = styled.div` 
width: 48px;
height: 48px;
margin-right: 15px;
`;
const AvaImg:StyledComponent<'img', any> = styled.img` 
width: 48px;
height: 48px;
border-radius: 50%;
object-fit:cover;
`;
const NameDiv:StyledComponent<'div', any> = styled.div` 
min-width: 48px;
min-height: 48px;
font-size: 24px;
font-weight: 500;
line-height: 28px;
color: #0C3953;
margin-right: 10px;
padding: 20px 5px 0 5px;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
`;
const EmailDiv:StyledComponent<'div', any> = styled.div` 
min-width: 48px;
min-height: 48px;
font-size: 12px;
padding: 40px 5px 0 5px;
color: #5e6c84;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
`;

export default FormPrivateData;
