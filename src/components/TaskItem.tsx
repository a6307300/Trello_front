/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useEffect} from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector} from 'react-redux';
import { CommentRootState } from 'typesafe-actions';
import { getComments } from '../store/commentSlice';
import { setActState, setColumn, setDescript, setRange, setTaskId, setTaskName } from '../store/taskModalSlice';



interface ITaskItem {
    key?: number,
    taskId: number,
    taskName:string,
    range: number,
    description: string,
    column: string,
    currentTask:number,
    setCurrentTask:any,
    setCurrentTaskColumn: number,
    dragTaskStartHandler:any,
    dragTaskEndHandler:any,
    dragTaskOverHandler: any,
    dropTaskHandler: any,
    currentTaskColumn: number,
  }

const TaskItem: FC<ITaskItem> = ({taskId, taskName, range, description, column, 
    dragTaskStartHandler, dragTaskEndHandler, dragTaskOverHandler, dropTaskHandler}) => {

    const dispatch = useDispatch();
    const comments = useSelector((state:CommentRootState) => state.comments);
    const commentsTask:number[] = Object.values(comments).flat().map( comm => comm.taskID==taskId? 1:0);
    const total = commentsTask.reduce(function(a:number, b:number) {
        return a + b;
    },0);

    const hidden = Boolean(localStorage.getItem('hideShared'));
    const hiddenRange = Boolean(range);
    const hiddenDescr = Boolean(description);
    const hiddenComment = Boolean(total);

    useEffect(() => {
        dispatch(getComments(localStorage.getItem('board') as string));
    }, []);

    const start = (e:React.DragEvent<HTMLDivElement>) => {
        dragTaskStartHandler(e, taskId, column);
    };

    const end = (e:React.DragEvent<HTMLDivElement>) => {
        dragTaskEndHandler(e);
    };

    const drop = (e:React.DragEvent<HTMLDivElement>) => {
        dropTaskHandler(e, taskId, column);
    };

    const over = (e:React.DragEvent<HTMLDivElement>) => {
        dragTaskOverHandler(e);
    };

    const setTaskModal = () => {
        dispatch(setTaskId(taskId));
        dispatch(setTaskName(taskName));
        dispatch(setDescript(description));
        dispatch(setRange(range));
        dispatch(setColumn(column));
        taskId?dispatch(setActState(true)):dispatch(setActState(false));
    };

    return (
        <TaskStyledDiv
            hidden={Boolean(taskId)}
            draggable={!hidden?true:false}
            onDragStart={start}
            // onDragLeave={}
            onDragEnd={end}
            onDragOver={over}
            onDrop={drop}
        >
            <TaskNameStyledDiv
                onClick={setTaskModal}
            >
                {hiddenRange?<StyledPActive>{taskName}</StyledPActive>:<StyledP>{taskName}</StyledP>}
                <IconsDiv
                    hidden={hiddenDescr||hiddenComment}
                >
                    <DescriptionIcon
                        hidden={hiddenDescr}
                    >
                        <img src='images/descr.png' />
                    </DescriptionIcon>
                    <DescriptionIcon
                        hidden={hiddenComment}
                    >
                        <img src='images/comment.png' />
                        {total}
                    </DescriptionIcon>
                </IconsDiv>
            </TaskNameStyledDiv>
        </TaskStyledDiv>
    );
};



const StyledP:StyledComponent<'p', any> = styled.p` 
text-decoration: line-through;
width: 230px;
min-height: 30px;
margin-bottom: 0;
margin-top: 0;
word-wrap : break-word;
margin-left: 1px;
opacity:80%;
border: none;
border-radius: 3px;
cursor: move;
`;

const StyledPActive:StyledComponent<'p', any> = styled.p` 
width: 230px;
min-height: 30px;
margin-bottom: 0;
margin-top: 0;
word-wrap : break-word;
margin-left: 1px;
border: none;
border-radius: 3px;
cursor: move;
`;

const TaskStyledDiv:StyledComponent<'div', any> = styled.div` 
width: 250px;
height: ${(props) => ((props.hidden) ? 'auto': '2px')};
margin-bottom: 8px;
margin-top: 2px;
margin-left: 10px;
word-wrap : break-word;
background-color: ${(props) => ((props.hidden) ? 'white': 'transparent')};
position: relative;
z-index: 1;
border: none;
border-radius: 3px;
cursor: move;
display: flex;
flex-direction: column;
align-items: center;
box-shadow: ${(props) => ((props.hidden) ? '0 4px 4px -4px rgba(0, 0, 0, .2);': 'none')};
`;
const TaskNameStyledDiv:StyledComponent<'div', any> = styled.div` 
width: 230px;
min-height: 35px;
margin-left: 10px;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
position: relative;
z-index: 1;
color: #172b4d;
display: flex;
flex-direction: column;
justify-content: space-between;
margin-top: 1px;
`;

const IconsDiv:StyledComponent<'div', any> = styled.div` 
height: 20px;
flex-direction: row;
justify-content: start;
align-items: center;
display: ${(props) => ((props.hidden) ? 'flex': 'none')};
`;


const DescriptionIcon:StyledComponent<'div', any> = styled.div` 
width: 25px;
height: 14px;
margin-right: 15px;
display: ${(props) => ((props.hidden) ? 'flex': 'none')};
justify-content: space-between;
color: grey;
`;

export default TaskItem;