/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/prop-types */
import React, { FC } from 'react';
import { useDispatch} from 'react-redux';
import { Route, Redirect } from 'react-router';
import { checkLogin } from '../store/userSlice';

interface IProtectedRoute {
    exact: boolean,
    path: string,
    component: any,
}
const ProtectedRoute: FC<IProtectedRoute> = ({component: Component, ...rest}) => {

    const dispatch = useDispatch();

    return (
        <Route 
            {...rest}
            render = {props => {
                dispatch(checkLogin());
                if(localStorage.getItem('name')) {
                    return <Component {...props} />;
                } else {
                    return <Redirect to ={
                        {
                            pathname:'/login',
                            state: {
                                from: props.location
                            }
                        }
                    }
                    />;
                }
            }
            }
        />
    );
};


export default ProtectedRoute;