/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useState} from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch} from 'react-redux';
import { changeComment, deleteComment } from '../store/commentSlice';
interface ICommentItem {
    commentId: number,
    comment: string,
    userID: string,
    taskID: number,
    key: number,
    authorName: string,
    authorAva: string,
  }
const CommentItem: FC<ICommentItem> = ({commentId, comment, authorName, authorAva}) => {

    const dispatch = useDispatch(); 

    const hidden = authorName==localStorage.getItem('name')?true:false;

    const delComment = () => {
        dispatch(deleteComment(commentId));
    };
    const [commentEdit, setCommentEdit] = useState<boolean>(false);
    const [inputComment, setInputComment] = useState<string>(comment);
    
    const showEditComment = () => {
        setCommentEdit(true);
    };
    const hideEditComment = () => {
        setCommentEdit(false);
    };
    const handleEditComment = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputComment(target.value);
    };  
    const saveComment = () => {
        if (inputComment!==''){
            const dataComment = {
                commentText:inputComment,
                id: commentId,
            };
            dispatch(changeComment(dataComment));
            hideEditComment();
        }
    };
    const EnterClick = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            saveComment();
        }
        if (e.key === 'Escape') {
            setInputComment(comment);
            hideEditComment();
        }
    };
    return (
        <CommentStyledDiv>
            <AvatarDiv>
                <AvaStyled src={authorAva} />
            </AvatarDiv>
            <TextAreaDiv>
                <CommentDiv
                    hidden={commentEdit}
                >
                    <CommentAuthorDiv>
                        {authorName}
                    </CommentAuthorDiv>
                    <CommentTextDiv>
                        {comment}
                    </CommentTextDiv>
                    <CommentButtons>
                        <CommentButton
                            hidden={hidden}
                            onClick={showEditComment}
                        >
                    Изменить
                        </CommentButton>
                        <CommentButton
                            hidden={hidden}
                            onClick={delComment}
                        >
                    Удалить
                        </CommentButton>
                    </CommentButtons>
                </CommentDiv>
                <CommentsInputDiv
                    hidden={commentEdit}
                >
                    <CommentsInput
                        onChange={handleEditComment}
                        onKeyDown={EnterClick}
                        value={inputComment}
                    />
                    <CommentSaveButton
                        onClick = {saveComment}
                    >Сохранить</CommentSaveButton>
                </CommentsInputDiv>
            </TextAreaDiv>
        </CommentStyledDiv>
    );
};

const AvaStyled:StyledComponent<'img', any> = styled.img` 
height: 35px;
width: 35px;
border-radius: 50%;
object-fit: cover;
`;
const CommentStyledDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
min-height: 90px;
display: flex;
flex-direction: row;
margin-top: 5px;
`;
const AvatarDiv:StyledComponent<'div', any> = styled.div` 
width: 55px;
min-height: 35px;
display: flex;
justify-content: center;
`;
const TextAreaDiv: StyledComponent<'div',any> = styled.div `
width: 90%;
display: flex;
flex-direction: column;
word-wrap : break-word;
`;
const CommentDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
display: flex;
flex-direction: column;
display: ${(props) => ((props.hidden) ? 'none': 'block')};
`;
const CommentAuthorDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
min-height: 20px;
`;

const CommentTextDiv:StyledComponent<'div', any> = styled.div` 
width: 93%;
min-height: 31px;
border-radius: 3px;
margin-top: 3px;
margin-bottom: 1px;
border: 1px solid grey;
background-color: white;
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
padding-top: 5px;
padding-left: 5px;
word-wrap : break-word;
`;
const CommentButtons:StyledComponent<'div', any> = styled.div` 
width: 100%;
min-height: 20px;
display:flex;
flex-direction: row;
`;
const CommentButton:StyledComponent<'button', any> = styled.button` 
width: 80px;
min-height: 20px;
border:none;
text-decoration: underline;
text-align: start;
display: ${(props) => ((props.hidden) ? 'flex': 'none')};
&:hover {
  color: grey;
  cursor: pointer;
}
`;
const CommentsInputDiv:StyledComponent<'div', any> = styled.div` 
width: 100%;
min-height: 60px;
margin-top: 3px;
margin-bottom: 3px;
border-radius: 3px;
border: 1px solid grey;
font-weight: normal;
color: grey;
display: flex;
flex-direction: column;
background-color: white;
display: ${(props) => ((props.hidden) ? 'block': 'none')};
word-wrap : break-word;
`;
const CommentsInput:StyledComponent<'input', any> = styled.input` 
width: 98%;
min-height: 30px;
border: none;
outline: none;
color: #172b4d;
font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif;
font-size: 14px;
font-weight: 400;
display: flex;
flex-flow: row wrap;
word-wrap : break-word;
`;
const CommentSaveButton:StyledComponent<'button', any> = styled.button` 
width: 100px;
height: 25px;
border: none;
background-color: silver;
border-radius: 3px;
margin-left: 1px;
`;

export default CommentItem;