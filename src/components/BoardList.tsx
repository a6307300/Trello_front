/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, FC } from 'react';
import styled, {StyledComponent} from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import BoardPreview from './BoardPreview';
import Header from './Header';
import {getBoards} from '../store/boardSlice';
import { BoardRootState, SharedRootState } from 'typesafe-actions';
import { getSharedBoards } from '../store/sharedBoardSlice';

const BoardList: FC = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBoards());
        dispatch(getSharedBoards());
    }, []);

    const boards = useSelector((state:BoardRootState) => state.boards);
    const shared = useSelector((state:SharedRootState) => state.shared);

    return(
        <BoardListSpace>
            <Header />
            <BoardsSpace
                hidden={localStorage.getItem('sharedBoards') as unknown as boolean} 
            >
                {Object.values(boards).flat().map((board) => {
                    return (
                        <BoardPreview 
                            key={board.id}
                            boardId={board.id}
                            boardName={board.boardName}
                            hidden={false}
                            authorName={localStorage.getItem('name') as string}
                            authorAva={localStorage.getItem('ava') as string}
                        />
                    );
                }
                )}
            </BoardsSpace>
            <SharedBoardsSpace
                hidden={localStorage.getItem('sharedBoards') as unknown as boolean}
            >
                {Object.values(shared).flat().map((share) => {
                    return (
                        <BoardPreview 
                            key={share.id}
                            boardId={share.id}
                            boardName={share.boardName}
                            hidden={true}
                            authorName={share.user.fullName}
                            authorAva={share.user.avatar}
                        />
                    );
                }
                )}
            </SharedBoardsSpace>
        </BoardListSpace>
    );
};

const BoardListSpace:StyledComponent<'div', any> = styled.div` 
width: 100%;
`;
const BoardsSpace:StyledComponent<'div', any> = styled.div` 
width: 100%;
display:flex;
flex-flow: row nowrap;
padding-left: 48px;
display: ${(props) => ((props.hidden) ? 'none': 'flex')};
`;

const SharedBoardsSpace:StyledComponent<'div', any> = styled.div` 
width: 100%;
display:flex;
flex-flow: row wrap;
padding-left: 48px;
display: ${(props) => ((props.hidden) ? 'flex': 'none')};
`;

export default BoardList;