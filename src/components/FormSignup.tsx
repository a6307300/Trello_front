/* eslint-disable no-constant-condition */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { FC, useState } from 'react';
import styled, { StyledComponent } from 'styled-components';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import FormSpace from './formComponents/FormSpace';
import LogoTrello from './formComponents/LogoStyled';
import ButtonForm from './formComponents/ButtonForm';
import { postUser } from '../store/userSlice';

const FormSignup: FC = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const [inputTextName, setInputTextName] = useState<string>('');
    const [inputTextEmail, setInputTextEmail] = useState<string>('');
    const [inputTextPassword, setInputTextPassword] = useState<string>('');

    const handleChangeName = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextName(target.value);
    };
    const handleChangeEmail = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextEmail(target.value);
    };
    const handleChangePassword = (e: React.SyntheticEvent) => {
        const target = e.currentTarget as HTMLInputElement;
        setInputTextPassword(target.value);
    };
  interface IDataUser {
    fullName: string;
    email: string;
    password: string;
  }
  const Registered = async (e: React.SyntheticEvent) => {
      e.preventDefault();
      if (inputTextName && inputTextEmail && inputTextPassword) {
          const aboutUser: IDataUser = {
              fullName: inputTextName,
              email: inputTextEmail,
              password: inputTextPassword,
          };
          await dispatch(postUser(aboutUser));
          setInputTextName('');
          setInputTextEmail('');
          setInputTextPassword('');
          history.push('/');
      } else {
          alert('Поля не должны быть пустыми');
      }
  };

  return (
      <CommonSpaceStyled>
          <LogoTrello />
          <FormSpace
              hiddenInputName={false}
              textHeader="Введите данные для регистрации"
              textFooter="У меня есть аккунт. Авторизация"
              link="/login"
              color="#5e6c84"
              handleChangeName={handleChangeName}
              handleChangeEmail={handleChangeEmail}
              handleChangePassword={handleChangePassword}
              inputTextName={inputTextName}
              inputTextEmail={inputTextEmail}
              inputTextPassword={inputTextPassword}
              hiddenAva={true}
          />
          <ButtonForm
              value="signup"
              label="Зарегистрироваться"
              functionClick={Registered}
          />
          {/* <a href='/'>Профиль</a> */}
      </CommonSpaceStyled>
  );
};

const CommonSpaceStyled: StyledComponent<'div', any> = styled.div`
width: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;

export default FormSignup;
