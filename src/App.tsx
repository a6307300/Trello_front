import React, { FC } from 'react';
import { Route } from 'react-router';
import FormSignup from './components/FormSignup';
import FormLogin from './components/FormLogin';
import FormPrivateData from './components/formPrivateData';
import { BrowserRouter } from 'react-router-dom';
import ProtectedRoute from './components/protectedRoute';
import BoardList from './components/BoardList';
import Board from './components/Board';

const App: FC = () => {
    return (
        <BrowserRouter>
            <Route path='/signup'component={FormSignup}/>
            <Route path='/login' component={FormLogin}/>
            <ProtectedRoute 
                exact
                path='/profile'
                component={FormPrivateData}
            />
            <ProtectedRoute 
                exact
                path='/'
                component={BoardList}
            />
            <ProtectedRoute 
                exact
                path='/board'
                component={Board}
            />
        </BrowserRouter>
    );
};

export default App;
